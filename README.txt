Running (Launching) Blackjack_WPL
---------------------------------

Author:  	William Paul Liggett (junktext@junktext.com)
Date:  		May 29th, 2018



Overview:
--------
Blackjack_WPL is a simple GUI application that simulates a game of Blackjack between you and a computerized
dealer.  This game was programmed in Java, thus the application should work on any platform that has a Java
Runtime Environment (JRE) installed.  Having a JRE means that you can run general-purpose Java (Java SE) 
programs.  You can easily test if you have a JRE by simply opening up a terminal window and entering "java" 
(without quotes) as a command.  If you receive a bunch of output such as various options you can include with 
"java", then your JRE is working.  Otherwise, if you see a message similar to "command not found", then your JRE
is not fully installed correctly.  You cannot run this game without an operational JRE on your computer.

To run the game, the instructions will vary per operating system (OS) platform.  As for this writing, here are
the quickest methods:

* Microsoft Windows:  Simply double-click the "Blackjack_WPL_v0.3.jar" file.

* Linux and UNIX (includes Mac OS X):  Open up a terminal (command prompt) window and enter the commands:

     cd ~/Downloads
     chmod u+x Blackjack_WPL_v0.3.jar
     java -jar Blackjack_WPL_v0.3.jar


That's it!


P.S. This program is Free/Libre and Open Source Software (F/LOSS).  Please feel free to study, modify, or share
the code and application with whomever you choose.  Though, please read the LICENSE.txt for specific details.

