import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class Blackjack_WPL_JUnit_Tests {
	
	/* --------------------- */
	/* BlackjackPlayer Tests */
	/* --------------------- */


	// Test Number 1
	@Test
	public void shouldCreateNewPlayer() {
		
		BlackjackPlayer testPlayer = new BlackjackPlayer("TestBot");


		// Syntax:  assertEquals(test_failure_msg, expected, actual);
		assertEquals("BlackjackPlayer Failure: A new player was not created.", "TestBot", testPlayer.getName());
		
	}


	// Test Number 2
	@Test
	public void newPlayerShouldHaveZeroPoints() {
		
		BlackjackPlayer testPlayer = new BlackjackPlayer("TestBot");


		// Syntax:  assertEquals(test_failure_msg, expected, actual);
		assertEquals("BlackjackPlayer Failure: Newly constructed player should have 0 points.", 0, testPlayer.getPoints());
		
	}



	// Test Number 3
	@Test
	public void shouldRenamePlayer() {
		
		BlackjackPlayer testPlayer = new BlackjackPlayer("TestBot");

		testPlayer.setName("AwesomeBot");


		// Syntax:  assertEquals(test_failure_msg, expected, actual);
		assertEquals("BlackjackPlayer Failure: Cannot rename the player.", "AwesomeBot", testPlayer.getName());
		
	}



	// Test Number 4
	@Test
	public void shouldResetPlayersCards() {
		
		BlackjackPlayer testPlayer = new BlackjackPlayer("TestBot");

		// Perform the method that we are trying to test!
		testPlayer.resetCards();

		// pointsTotal should always stay at zero (0), else the test fails.
		int pointsTotal = 0;

		// Traverse through the card array and add-up pointsTotal.
		int cardColumn = 0;
    	
	    	do {
	    		
	    		for (int cardRow = 0; cardRow <= 12; cardRow++) {
				// .showCard(row, column) will show whether the player has a card.
				// Zero (0) means that the player does not have that card.
	    			pointsTotal += testPlayer.showCard(cardRow, cardColumn);
	    		}
	    		
	    		// Move to the next column in the 'card' array.
	    		cardColumn++;
	    		
	    	} while (cardColumn <= 3);


		// Syntax:  assertEquals(test_failure_msg, expected, actual);
		assertEquals("BlackjackPlayer Failure: The cards were not reset.", 0, pointsTotal);
		
	}




	// Test Number 5
	@Test
	public void shouldReceiveCard() {
		
		BlackjackPlayer testPlayer = new BlackjackPlayer("TestBot");
		
		// Syntax:  receiveCard(int row, int column, int value);
		// Let's add a 5 of Hearts to the player.  (Row 3: 5 card, Column 0: Hearts, Value: 5).
		// The row is 3 because we start with the first row (0) storing the "2" card.
		testPlayer.receiveCard(3, 0, 5);


		// Syntax:  assertEquals(test_failure_msg, expected, actual);
		assertEquals("BlackjackPlayer Failure: Cannot receive a card correctly.", 5, testPlayer.showCard(3, 0));
		
	}





	// Test Number 6
	@Test
	public void shouldDetermineBestAceValues_OnlyOneAce_NoOtherCards() {
		
		BlackjackPlayer testPlayer = new BlackjackPlayer("TestBot");

		// Make sure the player's card array is initialized correctly.
		testPlayer.resetCards();

		
		// Syntax:  receiveCard(int row, int column, int value);
		// Add an Ace of Diamonds to the player.  (Row 12: Ace card, Column 1: Diamonds, Value: 11).
		// The default value for an Ace is 11.
		testPlayer.receiveCard(12, 1, 11);

		
		// Should keep the Ace to equal 11.
		testPlayer.determineBestAceValues();


		// Syntax:  assertEquals(test_failure_msg, expected, actual);
		assertEquals("BlackjackPlayer Failure: Best Ace value for one Ace, no other cards, is incorrect.", 11, testPlayer.getPoints());
		
	}







	// Test Number 7
	@Test
	public void shouldDetermineBestAceValues_TwoAces_NoOtherCards() {
		
		BlackjackPlayer testPlayer = new BlackjackPlayer("TestBot");

		// Make sure the player's card array is initialized correctly.
		testPlayer.resetCards();

		
		// Syntax:  receiveCard(int row, int column, int value);
		// Add an Ace of Diamonds to the player.  (Row 12: Ace card, Column 1: Diamonds, Value: 11).
		// The default value for an Ace is 11.
		testPlayer.receiveCard(12, 1, 11);

		// Add an Ace of Clubs to the player.  (Row 12: Ace card, Column 3: Clubs, Value: 11).
		// The default value for an Ace is 11.
		testPlayer.receiveCard(12, 3, 11);

		
		// We do not want 22 or 2, but 12 (11 + 1).
		testPlayer.determineBestAceValues();


		// Syntax:  assertEquals(test_failure_msg, expected, actual);
		assertEquals("BlackjackPlayer Failure: Best Ace value for two Aces, no other cards, is incorrect.", 12, testPlayer.getPoints());
		
	}





	// Test Number 8
	@Test
	public void shouldDetermineBestAceValues_FourAces_2and3cards() {

		// In short, this test means that the player will have four Aces (11 + 1 + 1 + 1 = 14) along with a "2" and a "3" card.
		// The total points should then be: 14 + 2 + 3 = 19.
		
		BlackjackPlayer testPlayer = new BlackjackPlayer("TestBot");

		// Make sure the player's card array is initialized correctly.
		testPlayer.resetCards();

		
		// Syntax:  receiveCard(int row, int column, int value);
		// Add an Ace of Hearts to the player.  (Row 12: Ace card, Column 0: Hearts, Value: 11).
		// The default value for an Ace is 11.
		testPlayer.receiveCard(12, 0, 11);

		// Add an Ace of Diamonds to the player.  (Row 12: Ace card, Column 1: Diamonds, Value: 11).
		// The default value for an Ace is 11.
		testPlayer.receiveCard(12, 1, 11);

		// Add an Ace of Spades to the player.  (Row 12: Ace card, Column 2: Spades, Value: 11).
		// The default value for an Ace is 11.
		testPlayer.receiveCard(12, 2, 11);

		// Add an Ace of Clubs to the player.  (Row 12: Ace card, Column 3: Clubs, Value: 11).
		// The default value for an Ace is 11.
		testPlayer.receiveCard(12, 3, 11);

		// Add a 2 of Diamonds to the player.  (Row 0: 2 card, Column 1: Diamonds, Value: 2).
		testPlayer.receiveCard(0, 1, 2);

		// Add a 3 of Clubs to the player.  (Row 1: 3 card, Column 3: Clubs, Value: 3).
		testPlayer.receiveCard(1, 3, 3);

		
		// Should change four Aces to be: 11 + 1 + 1 + 1 = 14.
		testPlayer.determineBestAceValues();

		
		// Again, once we add the 2 and the 3 card, the total should be 19.

		// Syntax:  assertEquals(test_failure_msg, expected, actual);
		assertEquals("BlackjackPlayer Failure: Best Ace value for four Aces, a 2 and a 3 card, is incorrect.", 19, testPlayer.getPoints());
		
	}








	/* ----------------- */
	/* DeckOfCards Tests */
	/* ----------------- */


	// Test Number 9
	@Test
	public void shouldCreateNewDeck() {
		
		DeckOfCards testDeck = new DeckOfCards();

		// The .getLastCardString() method, below, is meant to show which card was last taken from the deck.
		// With a new deck, this variable should be blank.

		// Syntax:  assertEquals(test_failure_msg, expected, actual);
		assertEquals("DeckOfCards Failure: The new deck was not created properly.", "", testDeck.getLastCardString());
		
	}




	// Test Number 10
	@Test
	public void shouldResetDeckCards() {
		
		DeckOfCards testDeck = new DeckOfCards();

		// Perfom the method that we are trying to test!
		testDeck.resetCards();

		// Initialize.
		int pointsTotal = 0;


		// Traverse through the card array, checking every card and adding-up pointsTotal.
		int cardColumn = 0;
    	
	    	do {
	    		
	    		for (int cardRow = 0; cardRow <= 12; cardRow++) {
				// .checkSpecificCard(row, column) will show whether the deck has that card.
				// Zero (0) means that the card was removed from the deck.
	    			pointsTotal += testDeck.checkSpecificCard(cardRow, cardColumn);
	    		}
	    		
	    		// Move to the next column in the 'card' array.
	    		cardColumn++;
	    		
	    	} while (cardColumn <= 3);


		// After checking every card from the deck we should have 380 points because:
		// (2+3+4+5+6+7+8+9+10+10+10+10+11)*4 = 380
		// There are four ten (10) values due to: card 10, Jack, Queen, and King.


		// Syntax:  assertEquals(test_failure_msg, expected, actual);
		assertEquals("DeckOfCards Failure: The cards were not reset properly.", 380, pointsTotal);
		
	}






	// Test Number 11
	@Test
	public void shouldGetRandomCard_Part1of2() {

		DeckOfCards testDeck = new DeckOfCards();

		// Make sure all the cards are in the proper place.
		testDeck.resetCards();
		
		// Grab a random card then remember where in the card array it was taken.
		int cardValue = testDeck.getRandomCard();
		int randomRow = testDeck.getLastCardRow();
		int randomColumn = testDeck.getLastCardColumn();

		// The first thing we'll test is whether the card taken from the deck is now gone (value of zero), 
		// which it should be from the .getRandomCard(); method.

		// Syntax:  assertEquals(test_failure_msg, expected, actual);
		assertEquals("DeckOfCards Failure: The random card taken was not removed from the deck properly.", 0, 
			     testDeck.checkSpecificCard(randomRow, randomColumn));
		
	}





	// Test Number 12
	@Test
	public void shouldGetRandomCard_Part2of2() {

		DeckOfCards testDeck = new DeckOfCards();

		// Make sure all the cards are in the proper place.
		testDeck.resetCards();
		
		// Grab a random card then remember where in the card array it was taken.
		int cardValue = testDeck.getRandomCard();
		int randomRow = testDeck.getLastCardRow();
		int randomColumn = testDeck.getLastCardColumn();


		// Now we'll test if the random card taken was assigned the correct Name and Suit,
		// which is easily known from the .getLastCardString(); method.

		// We will mimic the DeckOfCards logic by the code below, but we're using it differently by building an internal test String.

		
		// The statement below just sets it to be the initial value of the card, but we'll refine
		// it latter in the if-else-if statements underneath for the Jack, Queen, King, and Ace cards.
		String testString_Name = Integer.toString(cardValue);
    	
		// JACK
		if (randomRow == 9) {
			testString_Name = "Jack";
		}
		
		// QUEEN
		else if (randomRow == 10) {
			testString_Name = "Queen";
		}

		// KING
		else if (randomRow == 11) {
			testString_Name = "King";
		}
		
		// ACE
		else if (randomRow == 12) {
			testString_Name = "Ace";
		}
		
		else {
			// Do nothing!  Since we don't want to change lower card numbers to anything.
		}
		
		
		
		
		
		// This sets the artificial card icon (like a Heart symbol) via Unicode symbols.
		// The card column array order is arbitrarily set to: 0 = Hearts, 1 = Diamonds, 2 = Spades, and 3 = Clubs.

		// Initialize.
		String testString_Suit = "";
		

		// HEARTS
		if (randomColumn == 0) {
			testString_Suit = " \u2665";
		}
		
		// DIAMONDS
		else if (randomColumn == 1) {
			testString_Suit = " \u2666";
		}
		
		// SPADES
		else if (randomColumn == 2) {
			testString_Suit = " \u2660";
		}
		
		// CLUBS
		else {
			testString_Suit = " \u2663";
		}




		// Combine testString_Name + testString_Suit into simply 'testString'.
		String testString = testString_Name.concat(testString_Suit);


		// Okay now that the 'testString' is fully built.  Let's compare it to what the DeckOfCards reports.
		// They should be the same String.

		// Syntax:  assertEquals(test_failure_msg, expected, actual);
		assertEquals("DeckOfCards Failure: The random card taken did not build the correct String.", testString, 
				testDeck.getLastCardString());
		
		
	}

} // End of tests.
