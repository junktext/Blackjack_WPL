// File: DeckOfCards.java -- Creates a DeckOfCards object, meant to be used by a BlackjackPlayer object.

/* --------------------------------------------------------------------------------------------
    Blackjack_WPL: A simple game of Blackjack.
    Copyright (C) 2015 by William Paul Liggett (junktext@junktext.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License, version 3,
    as published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 -------------------------------------------------------------------------------------------- */


// This allows us to randomly choose a card out of the deck.
import java.security.SecureRandom;



public class DeckOfCards {
	
	private int[][] card;
	
	private String lastCardName, lastCardSuit;
	
	private int lastCardRow, lastCardColumn;
	
    // Create random numbers to choose a card.
	private SecureRandom randomNum = new SecureRandom();
	
	
	
	
	
	/* ----------- */
	/* CONSTRUCTOR */
	/* ----------- */
	
	public DeckOfCards() {
		
		// Creates an array that can hold 52 cards (standard deck).
		// 13 rows by 4 columns (Cards: 2 through Ace per each suit.)
		this.card = new int[13][4];
		
		this.lastCardName = "";
		
		this.lastCardSuit = "";
		
		// Note the zero (0) value stored below is only to make sure the vars are initialized.
		// You should run getRandomCard() at least once since this will set the lastCardRow/Column correctly.
		this.lastCardRow = 0;
		this.lastCardColumn = 0;
		
		// Initializes the new DeckOfCards to have the cards established right away.
		resetCards();
	}
	
	
	
	
	
	
	/* ------- */
	/* METHODS */
	/* ------- */
	
	// METHOD: Resets the deck of cards to put all the cards back in their proper place.
	public void resetCards() {
	
    	int cardColumn = 0;
    	
    	do {
			// We'll simply increase the card value from "2" up to "10" incrementally one at a time, but
			// once we encounter J, Q, and K we'll set these to equal "10".  Ace will be stored as "11", though,
			// we'll later show the user that the Ace equals "1" or "11" via the checkScores(); method.
    		
    		// The starting cardValue is "1" because it's just a starting point, since it will then first encounter
    		// the cardValue++ statement to make it "2" before storing the value to the DeckOfCard array.
    		int cardValue = 1;
    		
    		
    		// Note:
    		// cardRow = 0 means that the card stored (cardValue) will be "2".
			// cardRow = 9 (Jack), cardRow = 10 (Queen), cardRow = 11 (King), cardRow = 12 (Ace).
    		for (int cardRow = 0; cardRow <= 12; cardRow++) {
    			
    			if (cardRow < 9) {
    				// Cards: 2 through 10.
    				cardValue++;
    			}
    			
    			
    			else if ((cardRow >= 9) && (cardRow < 12)) {
    				// Cards: Jack, Queen, and King.
    				cardValue = 10;
    			}
    			
    			else {
    				// Card: Ace.
    				cardValue = 11;
    			}
    			
    			
    			// Assign the card to the Deck in the proper location of the array.
    			this.card[cardRow][cardColumn] = cardValue;	
    		}
    		
    		// Move to the next column in the 'card' array.
    		cardColumn++;
    		
    	} while (cardColumn <= 3);
	}
	
	
	
	
    // METHOD: This will get a random card from the deck (we'll use another method to give it to the Player/Dealer).
    // We'll be returning the int value of the card (randomCard) which will be useful for calculating the score.
	// Additionally, this method will store a String version of which randomCard was picked along with the suit,
	// which will be stored in the DeckOfCards attributes of lastCardName and lastCardSuit.
    public int getRandomCard() {
    	
    	int randomRow, randomColumn, randomCard;
    	
		// This do-while loop just makes sure we're not trying to grab a non-existent, zero (0), card from the deck.
    	// A zero (0) value card occurs when that card has been previously already taken out of the deck.
    	//
		// FOR FUTURE REFERENCE: If we ever want to expand this game to a lot more players, we may need to confirm
		// that all of the cards have not been handed out, else we'd get an infinite loop.
		do {
    		// Grab a card from the deck out of random.  This simulates shuffling the deck, but on an automatic basis :-).
    		randomRow = randomNum.nextInt(13);  // Ranges from 0-12 [not to 13].
    		randomColumn = randomNum.nextInt(4);  // Ranges from 0-3 [not to 4].
    		randomCard = this.card[randomRow][randomColumn]; // This will be the actual card value: 2, 8, Jack (10), etc.
		} while (randomCard == 0);  // Here's the test of whether the card value is zero (0), which we don't want.
	
		
		// These variables are useful for coordinating where a player should store their card that they receive from
		// this getRandomCard() method.  Because there are multiple "2" cards, for example, these help tell another
		// object (such as one based off of BlackjackPlayer) where that "2" card goes.
		this.lastCardRow = randomRow;
		this.lastCardColumn = randomColumn;
		
		
		// This is to provide an easier String output of which card was randomly given.
		// The statement below just sets it to be the initial value of the card, but we'll refine
		// it latter in the if-else-if statements underneath for the Jack, Queen, King, and Ace cards.
		this.lastCardName = Integer.toString(this.card[randomRow][randomColumn]);
		
		
    	// If you're wondering, the "Jack" is at row 9 because: row 0 = card 2, row 1 = card 3, ..., row 8 = card 10.
    	
		// JACK
		if (randomRow == 9) {
			this.lastCardName = "Jack";
		}
		
		// QUEEN
		else if (randomRow == 10) {
			this.lastCardName = "Queen";
		}

		// KING
		else if (randomRow == 11) {
			this.lastCardName = "King";
		}
		
		// ACE
		else if (randomRow == 12) {
			this.lastCardName = "Ace";
		}
		
		else {
			// Do nothing!  Since we don't want to change lower card numbers to anything.
		}
		
		
		
		
		
		// This sets the artificial card icon (like a Heart symbol) via Unicode symbols.
		// The card column array order is arbitrarily set to: 0 = Hearts, 1 = Diamonds, 2 = Spades, and 3 = Clubs.
		
		// HEARTS
		if (randomColumn == 0) {
			this.lastCardSuit = " \u2665";
		}
		
		// DIAMONDS
		else if (randomColumn == 1) {
			this.lastCardSuit = " \u2666";
		}
		
		// SPADES
		else if (randomColumn == 2) {
			this.lastCardSuit = " \u2660";
		}
		
		// CLUBS
		else {
			this.lastCardSuit = " \u2663";
		}
		
	
		
		
		// randomCard is what will be sent back as a result; however, we need to make sure to also put a zero (0)
		// value in for the card that was taken.  Thus, before we issue a "return" statement we'll do:
		this.card[randomRow][randomColumn] = 0;
		
		
		// Keep in mind that the above "card[randomRow][randomColumn] = 0" will not affect the previously stored
		// randomCard value that was set in the do-while loop at the beginning.  Therefore, we can now return this
		// card value back to the caller.
		return randomCard;
    }
	
	
	
    
    // METHOD: This will send back the String version of which card was last pulled out of the deck.  This makes it
    // easier for displaying information in the output.
    public String getLastCardString() {
    	return this.lastCardName.concat(this.lastCardSuit);
    }
    
    
    
    // METHOD: Helps to determine where in the array a random card, via getRandomCard(), was taken from.
    public int getLastCardRow() {
    	return this.lastCardRow;
    }
	
	
    
    // METHOD: Helps to determine where in the array a random card, via getRandomCard(), was taken from.
    public int getLastCardColumn() {
    	return this.lastCardColumn;
    }
    
    
    // METHOD: This is mainly used for unit testing DeckOfCards.
    // If the card was already taken, then a zero (0) value is returned.
    // Note: If a value of negative one (-1) is returned, then there is an error.
    public int checkSpecificCard(int row, int column) {
    	
    	// Initialize this to a non-existent value so that if we return this we know something went wrong.
    	int cardValue = -1;
    			
		// Apply two if checks to make sure we don't go beyond the array boundary.
		// row: Must be within 0-12 (13 cards, 2 through Ace).
		// column: Must be within 0-3 (Four suits).
		if ((row >= 0) && (row <= 12)) {
			if ((column >= 0) && (column <= 3)) {
				cardValue = this.card[row][column];
			}
		}
				
		// Send back the int value of what the card's value was within the if-statements above.
		return cardValue;
    }
    
	
}