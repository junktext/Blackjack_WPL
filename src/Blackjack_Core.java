// File: Blackjack_Core.java -- Drives all of the main logic of the program.
// However, this file relies upon the classes of BlackjackPlayer.java and DeckOfCards.java.

/* --------------------------------------------------------------------------------------------
    Blackjack_WPL: A simple game of Blackjack.
    Copyright (C) 2017 by William Paul Liggett (junktext@junktext.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License, version 3,
    as published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 -------------------------------------------------------------------------------------------- */

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

// For clickable buttons.
import javax.swing.JButton;

// To label "Blackjack", "You", "Dealer", "Points", and "Game Status".
import javax.swing.JLabel;

//To add scrollbars to the card output display (if necessary).
import javax.swing.JScrollPane;

//For the card output display.
import javax.swing.JTextArea;

// For GUI borders (for the card output displays).
import javax.swing.border.Border;
import javax.swing.BorderFactory;


public class Blackjack_Core extends JFrame implements ActionListener {
	
    /* ---------- */
    /* ATTRIBUTES */
    /* ---------- */	
	
    // These set up all the buttons that the user can click on with their mouse.
    private final JButton buttonHit, buttonStand, buttonNewGame;
    
    // These are the textual labels, such as "Blackjack", "You", "Dealer" and so forth.
    private final JLabel labelBlackjack, labelPlayer, labelDealer, labelPointsPlayer, labelPointsPlayerCONTENT,
                         labelPointsDealer, labelPointsDealerCONTENT, labelGameStatus, labelGameStatusCONTENT;
    
    // These are the player and dealer card output displays.
    private JTextArea outputPlayer, outputDealer;
       
    // GUI frame container
    private final Container containerGUI;
    
    // These are the three main objects that will be used throughout the game.
    private DeckOfCards deckOfCards;
    private BlackjackPlayer humanPlayer, computerPlayer;
    
    
    /* ----------- */
    /* CONSTRUCTOR */
    /* ----------- */

    // No-argument Constructor
    public Blackjack_Core() {
        // Set the title of the GUI window.
    	super("Programming Sample by William Paul Liggett");
    	
    	// Establish the three main objects used throughout the game.
        // Important: The player names MUST be unique! This is the value at: new BlackjackPlayer("playerName");
    	deckOfCards 	= new DeckOfCards();
    	humanPlayer 	= new BlackjackPlayer("You"); // The name will be: You.
    	computerPlayer 	= new BlackjackPlayer("Dealer"); // The name will be: Dealer.
    	
    	// GUI: Set up the main aspects.
        containerGUI = getContentPane();	
        containerGUI.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
				
        // GUI: To set the title of "Blackjack" to be very large.
        Font titleFont = new Font("sansserif", Font.BOLD, 30);

        // GUI: To set the size of what the cards' text and icons will be in the output.
        Font cardFont = new Font("sansserif", Font.BOLD, 24);

        // GUI: Make the font of buttons and primary labels to be larger looking.
        Font largerFont = new Font("sansserif", Font.BOLD, 16);

        // GUI: Set the font size of sub-labels like "Points:" and "Game Status:" to be small.
        Font smallFont = new Font("sansserif", Font.PLAIN, 16);
		
        // GUI: To have matching borders drawn for the player and dealer output areas.
        Border outputBorder= BorderFactory.createLineBorder(Color.BLACK, 1);

        // GUI: The textual label that says "Blackjack".
        labelBlackjack = new JLabel("Blackjack");
        labelBlackjack.setFont(titleFont);
        c.weightx = 0.0;
        c.fill = GridBagConstraints.CENTER;
        c.ipady = 40;      //make this component tall
        c.gridwidth = 5;
        c.gridx = 0;
        c.gridy = 0;
        c.insets = new Insets(20,0,20,0);  //top and bottom padding
        containerGUI.add(labelBlackjack, c);
        c.insets = new Insets(0,0,0,0);  //reset padding for other components

        // GUI: The textual label that says "You".
        labelPlayer = new JLabel(humanPlayer.getName());
        labelPlayer.setFont(largerFont);
        c.weightx = 0.0;
        c.fill = GridBagConstraints.CENTER;
        c.ipady = 5;
        c.gridwidth = 2;
        c.gridx = 0;
        c.gridy = 1;
        c.insets = new Insets(0,0,5,0);  //bottom padding
        containerGUI.add(labelPlayer, c);
        c.insets = new Insets(0,0,0,0);  //reset padding for other components

        // GUI: The output area for the player (for "You").
        outputPlayer = new JTextArea("", 5, 6); //5 rows with 6 columns
        outputPlayer.setFont(cardFont);
        // Make the background color to match the input display.
        //outputPlayer.setBackground(calcInputDisplay.getBackground());
        // To draw a small black border.
        outputPlayer.setBorder(outputBorder);
        c.weightx = 0.0;
        c.fill = GridBagConstraints.BOTH;
        c.ipady = 40;      //make this component tall
        c.gridwidth = 2;
        c.gridheight = 3;
        c.gridx = 0;
        c.gridy = 2;
        c.insets = new Insets(0,0,5,0);  //bottom padding
        //outputPlayer.addActionListener(this);  //JTextArea's can't use this method.
        outputPlayer.setEditable(false);
        containerGUI.add(outputPlayer, c);
        // Add scrollbars to the output display.
        JScrollPane outputYouScrollbars = new JScrollPane(outputPlayer, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
                                                          JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        containerGUI.add(outputYouScrollbars, c);
        c.insets = new Insets(0,0,0,0);  //reset padding
        c.gridheight = 1; //reset height
			
        // GUI: The textual label that says "Points:" (for the player).
        labelPointsPlayer = new JLabel("Points:");
        labelPointsPlayer.setFont(smallFont);
        c.weightx = 0.0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.ipady = 20;      
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 6;
        c.insets = new Insets(0,0,20,0);  //bottom padding
        containerGUI.add(labelPointsPlayer, c);
        c.insets = new Insets(0,0,0,0);  //reset padding for other components

        // GUI: The textual label that displays the CONTENT of the player's points.  This is your running score.
        labelPointsPlayerCONTENT = new JLabel("");
        labelPointsPlayerCONTENT.setHorizontalAlignment(JLabel.RIGHT);
        labelPointsPlayerCONTENT.setFont(smallFont);
        c.weightx = 0.0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.ipady = 20;      
        c.gridwidth = 1;
        c.gridx = 1;
        c.gridy = 6;
        c.insets = new Insets(0,0,20,0);  //bottom padding
        containerGUI.add(labelPointsPlayerCONTENT, c);
        c.insets = new Insets(0,0,0,0);  //reset padding for other components
		
        // GUI: The textual label that says "Dealer".
        labelDealer = new JLabel(computerPlayer.getName());
        labelDealer.setFont(largerFont);
        c.weightx = 0.0;
        c.fill = GridBagConstraints.CENTER;
        c.ipady = 5;      
        c.gridwidth = 3;
        c.gridx = 2;
        c.gridy = 1;
        c.insets = new Insets(0,120,5,0);  //left and bottom padding
        containerGUI.add(labelDealer, c);
        c.insets = new Insets(0,0,0,0);  //reset padding for other components
				
        // GUI: The output area for the dealer.
        outputDealer = new JTextArea("", 5, 9); //5 rows with 9 columns
        outputDealer.setFont(cardFont);
        // Make the background color to match the input display.
        //outputPlayer.setBackground(calcInputDisplay.getBackground());
        // To draw a small black border.
        outputDealer.setBorder(outputBorder);
        c.weightx = 0.0;
        c.fill = GridBagConstraints.BOTH;
        c.ipady = 40;      //make this component tall
        c.gridwidth = 2;
        c.gridheight = 3;
        c.gridx = 2;
        c.gridy = 2;
        c.insets = new Insets(0,120,5,0);  //left and bottom padding
        //outputDealer.addActionListener(this);  //JTextArea's can't use this method.
        outputDealer.setEditable(false);
        containerGUI.add(outputDealer, c);
        // Add scrollbars to the output display.
        JScrollPane outputDealerScrollbars = new JScrollPane(outputDealer, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
                                                             JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        containerGUI.add(outputDealerScrollbars, c);
        c.insets = new Insets(0,0,0,0);  //reset padding
        c.gridheight = 1; //reset height
				
        // GUI: The textual label that says "Points:" (for the dealer).
        labelPointsDealer = new JLabel("Points:");
        labelPointsDealer.setFont(smallFont);
        c.weightx = 0.0;
        c.fill = GridBagConstraints.BOTH;
        c.ipady = 20;
        c.gridwidth = 1;
        c.gridx = 2;
        c.gridy = 6;
        c.insets = new Insets(0,120,20,0);  //left and bottom padding
        containerGUI.add(labelPointsDealer, c);
        c.insets = new Insets(0,0,0,0);  //reset padding for other components
		
        // GUI: The textual label that displays the CONTENT of the dealer's points.  This is the dealer's running score.
        labelPointsDealerCONTENT = new JLabel("");
        labelPointsDealerCONTENT.setHorizontalAlignment(JLabel.RIGHT);
        labelPointsDealerCONTENT.setFont(smallFont);
        c.weightx = 0.0;
        c.fill = GridBagConstraints.BOTH;
        c.ipady = 20;
        c.gridwidth = 1;
        c.gridx = 3;
        c.gridy = 6;
        c.insets = new Insets(0,120,20,0);  //left and bottom padding
        containerGUI.add(labelPointsDealerCONTENT, c);
        c.insets = new Insets(0,0,0,0);  //reset padding for other components
				
        /* Note: */
        //Set the extra internal padding for the rest of the buttons to:
        c.ipady = 20;
        c.ipadx = 20;
		
        // GUI: Button "Hit"
        buttonHit = new JButton("Hit");
        buttonHit.setFont(largerFont);
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 0.0;
        c.weighty = 0.0;
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 7;
        c.insets = new Insets(0,0,20,0);  //bottom-only padding
        buttonHit.addActionListener(this);
        containerGUI.add(buttonHit, c);
		
        // GUI: Button "Stand"
        buttonStand = new JButton("Stand");
        buttonStand.setFont(largerFont);
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 0.0;
        c.gridwidth = 1;
        c.gridx = 1;
        c.gridy = 7;
        c.insets = new Insets(0,0,20,0);  //bottom-only padding
        buttonStand.addActionListener(this);
        containerGUI.add(buttonStand, c);
        //Reset padding for other buttons
        c.insets = new Insets(0,0,0,0);
				
        // GUI: Button "New Game"
        buttonNewGame = new JButton("New Game");
        buttonNewGame.setFont(largerFont);
        c.fill = GridBagConstraints.BOTH;
        //c.anchor = GridBagConstraints.CENTER;
        c.weightx = 0.0;
        c.gridwidth = 2;
        c.gridx = 2;
        c.gridy = 7;
        c.insets = new Insets(0,120,20,0);  //left and bottom padding
        buttonNewGame.addActionListener(this);
        containerGUI.add(buttonNewGame, c);
        //Reset padding for other buttons
        c.insets = new Insets(0,0,0,0);
				
        // GUI: The textual label that says "Game Status".
        labelGameStatus = new JLabel("Game Status:");
        labelGameStatus.setFont(smallFont);
        c.weightx = 0.0;
        c.fill = GridBagConstraints.BOTH;
        c.ipady = 40;      //make this component tall
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 8;
        c.insets = new Insets(40,0,20,0);  //top and bottom padding
        containerGUI.add(labelGameStatus, c);
        c.insets = new Insets(0,0,0,0);  //reset padding for other components
		
        // GUI: The textual label that contains the actual CONTENT of "Game Status".  Such as: "Your turn."
        labelGameStatusCONTENT = new JLabel("Your turn.");
        labelGameStatusCONTENT.setFont(smallFont);
        c.weightx = 0.0;
        c.fill = GridBagConstraints.BOTH;
        c.ipady = 40;      //make this component tall
        c.gridwidth = 3;
        c.gridx = 1;
        c.gridy = 8;
        c.insets = new Insets(40,0,20,0);  //top and bottom padding
        containerGUI.add(labelGameStatusCONTENT, c);
        c.insets = new Insets(0,0,0,0);  //reset padding for other components
    }
	
    
    /* ------- */
    /* METHODS */
    /* ------- */

    // METHOD:  This will give both the Player and Dealer two cards to begin the game.
    public void newGame() {
    	// This sets up the card arrays (Deck, Player, and Dealer) and clears out any old data.
    	resetGame();
    	
    	// The for-loop is to first deal out two cards to the Player, then two cards to the Dealer.
    	for(int i = 1; i <= 4; i++) {
			
            if(i <= 2) {
                // Give the Player two cards.
                getCard(humanPlayer.getName());
            }

            else {
                // Give the Dealer two cards.
                getCard(computerPlayer.getName());
            }
        }
    	
    	// See if either the Player or Dealer hit 21 on their first two cards, which ends the game.
    	checkScores();
    }
	    
    // METHOD: Respond to buttons that were clicked by the user.
    public void actionPerformed(ActionEvent event)
    {		
    	// THE PLAYER WANTS TO "HIT" (get another card).
    	// Step #1: Grab a random card from the Deck.
    	// Step #2: Put the card in the Player's card array (and remove the card from the Deck array).
    	// Step #3: Afterwards, determine if the Dealer needs another card, then see if anybody won.
    	if(event.getSource() == buttonHit) {
            getCard(humanPlayer.getName());
    		
            // Now, we'll implement the logic of whether the Dealer will take another card or not.
            // It goes like such:
            //   If Dealer's Points < 17, then the Dealer will be required to take another card no matter what.
            //   Else if Dealer's Points >= 17, then the Dealer will stand (not take another card) no matter what.
            if(computerPlayer.getPoints() < 17) {
                getCard(computerPlayer.getName());
                checkScores(); // See if anybody won.
            }

            else {
                // Don't take a card for the Dealer.
                checkScores(); // See if anybody won.
            }
        }
    	
    	
    	//  THE PLAYER WANTS TO "STAND" (doesn't want another card).
    	if(event.getSource() == buttonStand) {
            // Don't do anything for the Player; however...

            // Now, we'll implement the logic of whether the Dealer will take another card or not.
            // It goes like such:
            //   If Dealer's Points < 17, then the Dealer will be required to take another card no matter what.
            if(computerPlayer.getPoints() < 17) {
                getCard(computerPlayer.getName());
                checkScores(); // See if anybody won.
            }

            // We don't want to just hand the game to the Player if the Dealer has at least 17 but less than the Player.
            // Also notice that we're not using "<=" since the Dealer would like to tie rather than bust.
            else if(computerPlayer.getPoints() < humanPlayer.getPoints()) {
                getCard(computerPlayer.getName());
                checkScores(); // See if anybody won.
            }
    		
            // However, if the Dealer has at least 17 and also has more points than the Player, then the Dealer will stand.
            else {
                // Don't take a card.
                checkScores(); // See if anybody won.

                // Then we'll add in one extra score check so that we don't have a frozen game
                // in which both the Player and Dealer are continually standing.
                // The winner will then be whomever has the highest score.  And we'll see if it's a tie game.
                // We'll determine if we need to do this test if the "Hit" button is enabled, which would
                // be disabled if the game was over (somebody won or lost).
                if(buttonHit.isEnabled() == true) {

                    if(humanPlayer.getPoints() < computerPlayer.getPoints()) {
                        labelGameStatusCONTENT.setText("Dealer wins. (Both you and the dealer stands.)");
                    }

                    else if(humanPlayer.getPoints() == computerPlayer.getPoints()) {
                        labelGameStatusCONTENT.setText("Tie game. Nobody wins. (Both you and the dealer stands.)");
                    }

                    // Then we will disable the "Hit" and "Stand" buttons automatically.
                    buttonHit.setEnabled(false);
                    buttonStand.setEnabled(false);
                }
            }
    	}
    
    	
    	// THE PLAYER WANTS TO START A NEW GAME.
    	if(event.getSource() == buttonNewGame) {
            // Put all the cards back in the deck and reset all points and output displays.
            newGame();
    	}
    }
    
    
    // METHOD: This will simply put all the cards back to begin a new game.
    // It does this by defining multi-dimensional arrays (13 rows [2-Ace] x 4 columns [Hearts-Clubs]).
    public void resetGame() {
    	// Remove all the cards from the player and dealer.
    	humanPlayer.resetCards();
    	computerPlayer.resetCards();
    	
    	// Put back all the cards.
    	deckOfCards.resetCards();
    	
    	// Clear out any old data. 
    	humanPlayer.setPoints(0);
    	computerPlayer.setPoints(0);
    	outputPlayer.setText("");
    	outputDealer.setText("");
    	labelPointsPlayerCONTENT.setText("");
    	labelPointsDealerCONTENT.setText("");
    	labelGameStatusCONTENT.setText("Your turn.");	
    }
    
    
    // Gives a player a random card from the deck.
    // Input = The String result of BlackjackPlayer.getName();
    public void getCard(String playerName) {
        
        int cardValue = deckOfCards.getRandomCard();
        int randomRow = deckOfCards.getLastCardRow();
        int randomColumn = deckOfCards.getLastCardColumn();
        String cardString = deckOfCards.getLastCardString();
        
        // Checks whether the player is human.
        if(playerName.equals(humanPlayer.getName())) {
            // Give the player a random card from the deck.
            humanPlayer.receiveCard(randomRow, randomColumn, cardValue);
            
            // Update the text output of what card the player received.
            // The inside reference to .getText() means to append anything found in the text area.
            outputPlayer.setText(outputPlayer.getText() + cardString + "\n");
            
            // Update the textual label of the player's points.
            labelPointsPlayerCONTENT.setText(Integer.toString( humanPlayer.getPoints() ));  
        }
        
        else if(playerName.equals(computerPlayer.getName())) {
            // Give the dealer a random card from the deck.
            computerPlayer.receiveCard(randomRow, randomColumn, cardValue);

            // Update the text output of what card the dealer received.
            // The inside reference to .getText() means to append anything found in the text area.
            outputDealer.setText(outputDealer.getText() + cardString + "\n");

            // Update the textual label of the dealer's points.
            labelPointsDealerCONTENT.setText(Integer.toString( computerPlayer.getPoints() ));
        }
      
    }
    
    
    // METHOD: Compares the scores between the players to determine if anybody won or lost.
    public void checkScores() {
    	boolean disableHitandStand = false;
    	
    	// These methods will recalculate a player's score if they hold any Aces to
    	// see if the Ace(s) should be worth "1" or "11".
    	humanPlayer.determineBestAceValues();
    	computerPlayer.determineBestAceValues();
    	
        // Update the textual label of the human's points.
        labelPointsPlayerCONTENT.setText(Integer.toString( humanPlayer.getPoints() ));

        // Update the textual label of the dealer's points.
        labelPointsDealerCONTENT.setText(Integer.toString( computerPlayer.getPoints() ));

    	
    	// If the Dealer has 21 but the Player doesn't then the Dealer wins.
    	if((computerPlayer.getPoints() == 21) && (humanPlayer.getPoints() != 21)) {
            labelGameStatusCONTENT.setText("Dealer wins. Better luck next time.");
            disableHitandStand = true;
    	}
    	
    	
    	// If the Player has 21 but the Dealer doesn't then the Player wins.
    	else if((humanPlayer.getPoints() == 21) && (computerPlayer.getPoints() != 21)) {
    		labelGameStatusCONTENT.setText("You win!!!");
    		disableHitandStand = true;
    	}
    	
    	
    	// If both the Player and Dealer have 21, then it's a tie game.
    	else if((humanPlayer.getPoints() == 21) && (computerPlayer.getPoints() == 21)) {
            labelGameStatusCONTENT.setText("Tie game. Nobody wins.");
            disableHitandStand = true;
    	}
    	
    	
    	// If the Player has a score above 21, then they lose.
    	else if((humanPlayer.getPoints() > 21) && (computerPlayer.getPoints() < 21)) {
            labelGameStatusCONTENT.setText("You bust!  Sorry, you lose.");
            disableHitandStand = true;
    	}
    	
    	
    	// If the Dealer has a score above 21, then the Player wins.
    	else if((computerPlayer.getPoints() > 21) && (humanPlayer.getPoints() < 21)) {
            labelGameStatusCONTENT.setText("You win!!!  (Dealer busts.)");
            disableHitandStand = true;
    	}
    	
    	
    	// If both the Player and Dealer bust, then the Dealer wins.
    	else if((computerPlayer.getPoints() > 21) && (humanPlayer.getPoints() > 21)) {
            labelGameStatusCONTENT.setText("Dealer wins, even though both you and the dealer busts.");
            disableHitandStand = true;
    	}
    	
    	else {
            // Do nothing.
    	}
    	
    	
    	// Okay, if somebody won or lost, we'll disable the "Hit" and "Stand" buttons so
    	// that the only options are for the user to play a new game or to quit.
    	if(disableHitandStand == true) {	
            buttonHit.setEnabled(false);
            buttonStand.setEnabled(false);
    	}
    	
    	else {
            buttonHit.setEnabled(true);
            buttonStand.setEnabled(true);
    	}
    }
}