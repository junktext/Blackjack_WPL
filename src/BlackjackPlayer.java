// File: BlackjackPlayer.java -- Creates BlackjackPlayer objects, with each object representing a player.

/* --------------------------------------------------------------------------------------------
    Blackjack_WPL: A simple game of Blackjack.
    Copyright (C) 2017 by William Paul Liggett (junktext@junktext.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License, version 3,
    as published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 -------------------------------------------------------------------------------------------- */

public class BlackjackPlayer {

    private String name;	
    private int[][] card;
    private int points;

    /* ----------- */
    /* CONSTRUCTOR */
    /* ----------- */
    public BlackjackPlayer(String name) {

        this.name = name; 		
        this.card = new int[13][4];
        this.points = 0;

        // Initialize the player's cards that they have all to zero (0) to begin with.
        resetCards();
    }


    /* ------- */
    /* METHODS */
    /* ------- */	

    // METHOD: Sends back what the player calls themself.
    public String getName() {
        return this.name;
    }

    // METHOD: Allow the ability to change the name of the player.  Such as: "Bob", "William", and so forth.
    public void setName(String name) {
        this.name = name;
    }

    // METHOD: Sends back the player's current score.
    public int getPoints() {
        return this.points;
    }

    // METHOD: Change the player's earned points.
    public void setPoints(int points) {
        this.points = points;
    }

    // METHOD: Shows whether a player has a card at a particular array location.
    // If the value returned is zero (0) it means the player does not have that card. 
    // This is mainly useful for unit testing other methods like resetCards().
    public int showCard(int row, int column) {
        // This is both an initialization and a safeguard.  It does this by setting the player
        // to not have the card yet, which we'll later change via the 'if' statements below.
        // The safeguard comes by if a card requested is out of the array boundary, 
        // so then logically the player doesn't have a non-existent card!  :-)  
        // So, in that case, we'll simply return a zero (0) card value too.
        int cardValue = 0;

        // Apply two if checks to make sure we don't go beyond the array boundary.
        // row: Must be within 0-12 (13 cards, 2 through Ace).
        // column: Must be within 0-3 (Four suits).
        if((row >= 0) && (row <= 12)) {
            if((column >= 0) && (column <= 3)) {
                cardValue = this.card[row][column];		
            }
        }

        return cardValue;		
    }


    // METHOD: Resets the player's card array so that they have no cards to begin with (all zero values).
    public void resetCards() {
        int cardColumn = 0;

        do {

            for(int cardRow = 0; cardRow <= 12; cardRow++) {
                this.card[cardRow][cardColumn] = 0;
            }

            // Move to the next column in the 'card' array.
            cardColumn++;

        } while(cardColumn <= 3);
    }


    // METHOD: This will add a card obtained from the DeckOfCards (via its getRandomCard(); method).
    public void receiveCard(int row, int column, int value) {
        // Add the card to the proper array location.
        this.card[row][column] = value;

        // Update the player's total points.
        this.points += value;
    }


    // METHOD: Determines and sets the best score for the player if they have any Ace cards.
    // This is because an Ace can be either worth "1" or "11" points, whichever works better for the player.
    // Nothing happens if the player does not have any Aces.
    public void determineBestAceValues() {
        int cardColumn = 0, aceCount = 0;

        // Find out how many Aces the player holds. Max: 4.
        do {
            // Row 12 is where the Aces are held.  The "> 0" means that an Ace is present.
            if(this.card[12][cardColumn] > 0) {
                // Increase the aceCount.
                aceCount++;

                // Additionally, let's temporarily set any Ace found to equal "11",
                // which will help with our calculations below since we'll have a standard baseline
                // that we know how to manipulate the scores where need-be.
                this.card[12][cardColumn] = 11;
            }

            // Look at the next column in the 'card' array.
            cardColumn++;

        } while(cardColumn <= 3);


        // Now that we know how many Aces they player has, let's do some calculations.
        // If: No Aces.
        if(aceCount == 0) {
            // Do nothing and exit.
            return;
        }

        // Has at least one Ace.
        else {
            // Let's recalculate the player's points without any Aces.  This baselines the sum correctly.
            this.points = 0;
            cardColumn = 0;

            do {
                // Note: We're only going to row ELEVEN (as row 12 = Ace), which we don't want to add-in yet.
                for(int cardRow = 0; cardRow <= 11; cardRow++) {
                    if(this.card[cardRow][cardColumn] > 0) {
                        this.points += this.card[cardRow][cardColumn]; // Adds any cards found to their 'points'.
                    }
                }

                // Move to the next column in the 'card' array.
                cardColumn++;

            } while(cardColumn <= 3);

            // Alright, now that the player's score is correct, except for the Aces found, let's now perform some Ace mathematics.

            // One Ace.
            if(aceCount == 1) {
                // If the Ace will make the player bust, then the Ace will equal 1.
                if((this.points + 11) > 21) {
                    this.points += 1;
                }

                // Otherwise, the Ace will equal 11 to make their score better.
                else {
                    this.points += 11;
                }
            }

            // Two Aces.
            else if(aceCount == 2) {
                // If a single Ace will make the player bust, then each Ace will equal 1 (Meaning: +2 points).
                // Though, if they bust anyways from +2 points, then that can and should happen.
                if((this.points + 11) > 21) {
                    this.points += 2;
                }

                // Otherwise, if setting one Ace to "11" and the other to "1" is for the best then +12 points.
                // Logically, this means: ((this.points + 12) <= 21) but we don't need anything beyond this
                // else-statement to achieve this effect.  This is because you do not want two "11" value Aces,
                // as this will equal "22" which is a bust!
                else {
                    this.points += 12;
                }
            }

            // Three Aces.
            else if(aceCount == 3) {
                // If a single Ace will make the player bust, then each Ace will equal 1 (Meaning: +3 points).
                // Though, if they bust anyways from +3 points, then that can and should happen.
                if((this.points + 11) > 21) {
                    this.points += 3;
                }

                // Otherwise, if setting one Ace to "11" and the others to "1" is for the best then +13 points.
                // Logically, this means: ((this.points + 13) <= 21) but we don't need anything beyond this
                // else-statement to achieve this effect.  This is because you do not want two "11" value Aces,
                // as this will equal "22", along with an extra "1" value, which is a bust!
                else {
                    this.points += 13;
                }
            }

            // Four Aces.
            else {
                // If a single Ace will make the player bust, then each Ace will equal 1 (Meaning: +4 points).
                // Though, if they bust anyways from +4 points, then that can and should happen.
                if((this.points + 11) > 21) {
                    this.points += 4;
                }

                // Otherwise, if setting one Ace to "11" and the others to "1" is for the best then +14 points.
                // Logically, this means: ((this.points + 14) <= 21) but we don't need anything beyond this
                // else-statement to achieve this effect.  This is because you do not want two "11" value Aces,
                // as this will equal "22", along with two extra "1" values, which is a bust!
                else {
                    this.points += 14;
                }
            }
        }
    }
}