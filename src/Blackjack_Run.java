// File: Blackjack_Run.java -- Used to create a GUI window frame which runs Blackjack_Core.java.

/* --------------------------------------------------------------------------------------------
    Blackjack_WPL: A simple game of Blackjack.
    Copyright (C) 2017 by William Paul Liggett (junktext@junktext.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License, version 3,
    as published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 -------------------------------------------------------------------------------------------- */

import javax.swing.JFrame;

public class Blackjack_Run {
   public static void main(String[] args) {
      Blackjack_Core windowFrame = new Blackjack_Core(); 
      windowFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      
      // Set the application window to be: 820x670 pixels 
      windowFrame.setSize(820, 670); 
      windowFrame.setVisible(true); 
      
      // This will run the WPL-created method of giving two cards to the Player and Dealer initially.
      windowFrame.newGame();
   } 
}