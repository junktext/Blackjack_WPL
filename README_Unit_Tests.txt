Unit Tests for Blackjack_WPL
----------------------------

Utilizing:	JUnit version 4.12 (http://junit.org/)
Date:  		27 May 2015
Tester:  	William Paul Liggett (junktext@junktext.org)


Overview:
--------
Various automated unit tests were created for the Blackjack_WPL program.  The main areas of concern focus
on the BlackjackPlayer and the DeckOfCards classes as they are what the whole program relies upon.  For
example, the tests will confirm that objects created from these classes were instantiated (born) correctly,
that the card arrays and points are in sync, and other logic like determining the best Ace values are
assigned if a player holds one or more Ace cards.

Aspects like how the GUI works as well as the associated event-driven programmatic code (such as 
Blackjack_Core) are not meant to be unit tested since they rely upon user interaction.  Furthermore, 
standard getter and setter methods are not necessarily included unless it deals with more advanced 
capabilities.  This is because testing a general getter or setter is effectively just testing whether the 
Java Virtual Machine (JVM) is working correctly, which is a job for Oracle.

Please note that the tests are meant to be executed within a terminal (command prompt) window.  The tests
should work on any platform that has a working Java Runtime Environment (JRE) installed.  Having a JRE
means that you can run general-purpose Java (Java SE) programs.  You can easily test if you have a JRE by
simply opening up a terminal window and entering "java" (without quotes) as a command.  If you receive
a bunch of output such as various options you can include with "java", then your JRE is working.  Otherwise,
if you see a message similar to "command not found", then your JRE is not fully installed correctly.

You can download and use a JRE such as IcedTea, Zulu, or the standard Oracle JRE.  This README will not
attempt to explain how to do this for your platform.  Also, if you wish to enhance the JUnit tests or
any of the other source code to Blackjack_WPL, then you'll also need a Java Development Kit (JDK).
Again, you can use a JDK via IcedTea or Zulu (which both are implementations of OpenJDK) or you may use
the standard Oracle JDK.  Please search the Internet for further instructions via https://duckduckgo.com.

If you are confused between the terms of JDK, JRE, and JVM, the relationship is as such:
The JDK allows you to build programs for the JRE, and the JRE executes programs within the JVM.
Therefore:  JDK  -->  JRE  -->  JVM


Platform-Independent Instructions:
---------------------------------
Step 0:  Open up a terminal and navigate to the "Blackjack_WPL" folder and then the "JUnit Tests" folder.



OPTIONAL STEP -- Only necessary if you make changes to the provided JUnit tests.
Step 1:  Recompile the tests by issuing this command:

         javac -cp .:junit-4.12.jar Blackjack_WPL_JUnit_Tests.java

         (Note: There will be no output if the compilation went successfully.)



Step 2:  Run the tests by now entering this command:

         java -cp .:junit-4.12.jar:hamcrest-core-1.3.jar org.junit.runner.JUnitCore Blackjack_WPL_JUnit_Tests




If all went well, you should see output similar to:

         JUnit version 4.12
         ............
         Time: 0.045

         OK (12 tests)


That's it!

